# FLISOL Ecuador 2020 (virtual)

fecha: 17-Mayo-2020 16:55

![](img/logo-flisol-ecuador_bgd.png)

## Resumen
La ponencia está orientada a usuarios de R y QGIS. Se muestra el procedimiento para integrar scripts de R en QGIS usando el complemento _[Processing R Provider](https://north-road.github.io/qgis-processing-r/). El objetivo de la charla es incentivar a los usuarios de R que necesiten hacer tareas de análisis estadístico en QGIS.

## Créditos:

* R project: https://r-project.org
* QGIS project: https://qgis.org
* Processing R provider: https://north-road.github.io/qgis-processing-r/
* Rstudio: https://rstudio.com
* Paquete Xaringan: https://github.com/yihui/xaringan


## Licencia

<a rel="license" href="http://creativecommons.org/licenses/by/3.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/3.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/3.0/">Creative Commons Attribution 3.0 Unported License</a>.
